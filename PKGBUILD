# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: Steven Honeyman <stevenhoneyman at gmail com>

pkgname=qphotorec
pkgver=7.2
pkgrel=11
pkgdesc="Checks and undeletes partitions. Includes PhotoRec signature based recovery tool."
arch=('i686' 'x86_64' 'aarch64')
url="https://www.cgsecurity.org/wiki/PhotoRec"
license=('GPL')
makedepends=('qt5-tools' 'ntfs-3g' 'libewf' 'imagemagick')
conflicts=('testdisk' 'testdisk-wip')
provides=("testdisk=${pkgver}" 'photorec' 'fidentify')
source=("https://metainfo.manjariando.com.br/${pkgname}/org.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/org.${pkgname}.desktop"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}.png"
        "https://www.cgsecurity.org/testdisk-${pkgver}-WIP.tar.bz2"
        "org.freedesktop.policykit.${pkgname}.policy")
sha256sums=('89b3f261887dbfaea3e8ca2c3d73d0adf34b7b87b2bbeb336f092e1c49223eb3'
            '49d63b96d44af610c2fb4b40ee683369f24824eaf571f2183a8e4520e3ad5afe'
            '255d1bf712671bc1e11eeb0ea3a156958a41b9c818a0970f96482b05c0e199cc'
            '501df617fe1541d66c34fc6337dfc8b76f3e862ce55e3d19ccb3e29ced430f57'
            'd9a349ca318979c117e580917fd03c69129d97350cb6510db621ca1b3c8e829e')

prepare() {
    cd "${srcdir}/testdisk-${pkgver}-WIP"

    # Some fixes to make it Qt5 capable
    sed -i '/<QWidget>/d' src/*.h
    sed -i '/<QWidget>/d' src/*.cpp
    sed -i 's/Qt::escape\(.*\)text())/QString\1text()).toHtmlEscaped()/g' src/${pkgname}.cpp

    ## You might not need these, but I did for some reason
    test -f src/gnome/help-about.png || cp /usr/share/icons/gnome/48x48/actions/help-about.png src/gnome/
    test -f src/gnome/image-x-generic.png || cp /usr/share/icons/gnome/48x48/mimetypes/image-x-generic.png src/gnome/
}

build() {
    cd "${srcdir}/testdisk-${pkgver}-WIP"
    export QTGUI_LIBS="$(pkg-config Qt5Widgets --libs)"
    export QTGUI_CFLAGS="-fPIC $(pkg-config Qt5Widgets --cflags)"

    # add --disable-qt if you do not have qt5-base
    ./configure --prefix=/usr --enable-sudo
    make
}

package() {
    depends=('libjpeg' 'qt5-base')
    optdepends=('libewf: support EnCase files'
                'ntfs-3g: support NTFS partitions')

    cd "${srcdir}/testdisk-${pkgver}-WIP"
    make DESTDIR="${pkgdir}" install

    install -Dm644 "${srcdir}/org.freedesktop.policykit.qphotorec.policy" "${pkgdir}/usr/share/polkit-1/actions/org.freedesktop.policykit.qphotorec.policy"
    
    # Appstream
    rm -rf "${pkgdir}/usr/share/applications/${pkgname}.desktop"
    install -Dm644 "${srcdir}/org.${pkgname}.desktop" "${pkgdir}/usr/share/applications/org.${pkgname}.desktop"
    install -Dm644 "${srcdir}/org.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/org.${pkgname}.metainfo.xml"

    for size in 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/${pkgname}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done
}
